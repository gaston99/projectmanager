﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{    public class Conocimiento : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }

        public Disponibilidad Disponibilidad { get; set; }
        public IList<Instructor> Instructores { get; set; }
        public Demanda Demanda { get; set; }

        public IList<ConocimientoPorPrecio> ConocimientoPorprecio { set; get; }
        
        public IList<Archivo> Archivos { get; set; }

        public Conocimiento(String nombre)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
        }

        public void AgregarConocimientoPorPrecio(Nivel nivel, Double precio, string moneda)
        {
            if (ConocimientoPorprecio == null)
            {
                ConocimientoPorprecio = new List<ConocimientoPorPrecio>();
            }
            ConocimientoPorprecio.Add(new ConocimientoPorPrecio
            {
                Fecha = DateTime.Now,
                Nivel = nivel,
                Precio = new Precio(precio, moneda)
            });
            //else
            //{
              //  var precioMinimo = ConocimientoPorprecio.Min(x => x.Precio);
                //var precioMaximo = ConocimientoPorprecio.Max(x => x.Precio);

                //foreach (var C in ConocimientoPorPrecio)
                //{
                  //  if (C.Nivel == nivel) { }
                //}
            }

        }
}
