﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Clientes.Test
{
    [TestClass]
    public class UnitTestClientes
    {
        [TestMethod]
        public void PROBAR_CREAR_UNA_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            string direccionPrueba = "domicilio1, localidad1, provincia1, pais1";
            Assert.AreEqual(direccionPrueba, direccion.toString());
        }


    }
}
