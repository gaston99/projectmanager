﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public enum NivelConocimiento
    {
        Junior,
        Senior,
        Semisenior,
    }
}
