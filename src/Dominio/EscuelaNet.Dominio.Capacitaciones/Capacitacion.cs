﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Capacitacion : Entity, IAggregateRoot
    {
        public Instructor Instructor { get; set; }
        private int Duracion { get; set; }
        public void SetDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Error La Duracion de la Capacitacion no puede ser 0");
            Duracion = duracion;
        }
        public int GetDuracion()
        {
            return Duracion;
        }
        public DateTime MomentoInicio { get; set; }
        public IList<Alumno> Alumnos { get; set; }

        public void PushAlumno(string nombre)
        {
            if (this.Alumnos == null)
            {
                this.Alumnos = new List<Alumno>();
            }

            this.Alumnos.Add(new Alumno(nombre));
        }

        public IList<Conocimiento> Conocimientos { get; set; }

        public void PushConocimiento(Conocimiento conocimiento)
        {
            if (this.Conocimientos == null)
            {
                this.Conocimientos = new List<Conocimiento>();
            }
            this.Conocimientos.Add(new Conocimiento(conocimiento));
        }

        public void AgregarConocimientoAlAlumno(string nombre, Conocimiento conocimiento)
        {
            foreach (var alumno in Alumnos)
            {
                if (alumno.Nombre == nombre)
                {
                    alumno.Conocimientos.Add(conocimiento);
                }
            }

        }
    }
}
