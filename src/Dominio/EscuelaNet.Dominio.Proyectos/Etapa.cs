﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public class Etapa
    {
        public string Nombre { get => Nombre; set => Nombre = value; }
        public int Duracion { get => Duracion; set => Duracion = value; }
        public DateTime FechaInicio { get => FechaInicio; set => FechaInicio = value; }
        public DateTime FechaFin { get => FechaFin; set => FechaFin = value; }

        private Etapa()
        {
            this.Duracion = 0;
        }

        public Etapa(string nombre)
        {
            this.Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
        }
        public Etapa(string nombre, int duracion)
        {
            this.Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
            FechaInicio = DateTime.Today;
            CambiarDuracion(duracion);

        }



        public void CambiarDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Una etapa debe tener más de 0 días.");
            this.Duracion = duracion;
            calcFechaFin();//this.FechaInicio);
        }

        public void calcFechaFin()//DateTime fechaini)
       {
            //Calculo de los dias habiles que tarda la etapa y se calcula FechaFin(etapa)
            int contador = 1;
            string dia;
      
            DateTime fechaini;
            fechaini = this.FechaInicio;
            dia = fechaini.DayOfWeek.ToString("d");

            while (contador <= this.Duracion)
            {
                if ((dia != "5") && (dia != "6"))
                {
                    contador++;
               }
                fechaini = fechaini.AddDays(1);
                dia = fechaini.DayOfWeek.ToString("d");
            }

            this.FechaFin = fechaini.Date;
        }
    }
}


