﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Proyectos
{
    public enum EstadoDeProyecto
    {
        Diseno,
        Iniciado,
        Finalizado
    }
}
