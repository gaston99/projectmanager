﻿using System;
using EscuelaNet.Dominio.Proyectos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Capacitaciones.Test
{
    [TestClass]
    public class UnitTestCapacitaciones
    {
        [TestMethod]
        public void PROBAR_CAPACITACION()
        {
            var capa = new Capacitacion();
            Assert.AreEqual(0, capa.GetDuracion());
            Action action = () =>
            {
                capa.SetDuracion(-10);

            };
            Assert.ThrowsException<ExcepcionDeProyectos>(action);
        }

        [TestMethod]
        public void ADD_ALUMNO()
        {
            var capa = new Capacitacion();

            capa.PushAlumno("matias");
            Assert.AreEqual("matias", capa.Alumnos[0].Nombre);

        }

        [TestMethod]
        public void ADD_CONOCIMIENTO()
        {
            var capa = new Capacitacion();

            var conocimiento = new Conocimiento
            {
                Descripcion = "MVC Basico",
                Nivel = 1,
            };

            capa.PushConocimiento(conocimiento);

            Assert.AreEqual("MVC Basico", capa.Conocimientos[0].Descripcion);

        }

        [TestMethod]
        public void ADD_CONOCIMIENTO_AL_ALUMNO()
        {
            var capa = new Capacitacion();

            capa.PushAlumno("matias");

            var conocimiento = new Conocimiento
            {
                Descripcion = "MVC Basico",
                Nivel = 1,
            };

            capa.AgregarConocimientoAlAlumno("matias", conocimiento);


            Assert.AreEqual("MVC Basico", capa.Alumnos[0].Conocimientos[0].Descripcion);

        }
    }
}
